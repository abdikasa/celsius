package sheridan;

public class Celsius {

	
	public static int fromFahrenheit(int degree) {
		System.out.println((5 * (degree - 32)) / 9);
		
		
		if(degree < -460) {
			throw new NumberFormatException("Error: The highest temperature is reached at -459.67 degrees Fahrenheit (-273.15 degrees Celsius).");
		}
		
		return (5 * (degree - 32)) / 9;
	}
	
}
