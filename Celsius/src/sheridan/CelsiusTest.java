package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 * @author Abdi_ or Said Abdikarim or abdikasa
 * 991523345
 */


public class CelsiusTest {

	/**
	 * Initial test should fail.
	 * Convert Celsius.
	 */
	
	@Test
	public void testFromFahrenheitRegularPass() {
		boolean conversion = Celsius.fromFahrenheit(32) == 0;
		assertTrue("Fahrenheit to Celsis conversion should work", conversion == true);
	}
	
	@Test
	public void testFromFahrenheitRegularFail() {
		boolean conversion = Celsius.fromFahrenheit(32) == 0;
		fail("conversion should fail");
	}
	
	
	@Test()
	public void testFromFahrenheitExceptionalFail() {
		boolean conversion = Celsius.fromFahrenheit(-500) < -460;
		fail("Test should fail, conversion is invalid");
	}
	
	@Test(expected=NumberFormatException.class)
	public void testFromFahrenheitExceptionalPass() {
		boolean conversion = Celsius.fromFahrenheit(-500) < -460;
		fail("Test should fail, conversion is invalid");
	}
	
	
	@Test()
	public void testFromFahrenheitBIFail() {
		boolean conversion = Celsius.fromFahrenheit(-41) < -0;
		fail("Test should fail, conversion is invalid");
	}
	
	@Test()
	public void testFromFahrenheitBIPass() {
		boolean conversion = Celsius.fromFahrenheit(-41) == 0 ;
		assertEquals("test should pass", conversion);
	}
	
	
	@Test()
	public void testFromFahrenheitBOFail() {
		int conversion = Celsius.fromFahrenheit(-32);
		fail("test should fail");
		
	}
	
	
	@Test(expected=NumberFormatException.class)
	public void testFromFahrenheitBOPass() {
		int conversion = Celsius.fromFahrenheit(-32);
		assertEquals("test should pass", conversion ==  0);
	}
	
	

}
